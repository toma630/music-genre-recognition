# WEB Application : Music genres recognition

The idea is to create a web application that allows someone to upload a music and display the musical genre it belongs to with the help of an artificial intelligence.

## Run the app

To launch the web application, execute the python script `app.py` and then connect to the site via the address `localhost:5000`.

Import a music in mp3 or wav format of more than 20 seconds, the genre of the music and an adapted frequency profile will be proposed to you.

## Required packages

Install packages in terminal via running the following command `$ pip install -r requirements.txt`.

Installation of ffmpeg :

* Windows : Follow indications of [this tutorial](https://www.wikihow.com/Install-FFmpeg-on-Windows)
* Mac : run `brew install ffmpeg`

## useful links

* [GTZAN genre collection](http://marsyas.info/index.html) - Used database
* [Keras](https://keras.io/) - Keras documentation 

## Auteurs

* **Thomas Segré** 
* **Adrien Carrel** 
